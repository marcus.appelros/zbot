package main

import (
	"flag"
	"fmt"
	"math/big"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	zbot "./contracts/build"
	"github.com/bwmarrin/discordgo"
)

var (
	Token    string
	instance *zbot.Zbot
	uisub    int
	uisub1   int
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	instance = loadContract()

	go listenTelegram()
	go listenReddit()

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages | discordgo.IntentsDirectMessages | discordgo.IntentsGuilds | discordgo.IntentsGuildPresences)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	fmt.Println("Read message ", m.Content, " from ", m.Author.Username, "with ID", m.Author.ID)
	// If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}

	spl := strings.Split(m.Content, " ")
	if spl[0] == "Zblock" || spl[0] == "/block" || spl[0] == "/block@Zbot" {
		s.ChannelMessageSend(m.ChannelID, "The current Coston block is: "+currentBlock())
	} else if spl[0] == "Zregister" || spl[0] == "/register" || spl[0] == "/register@Zbot" {
		id := "discord " + m.Author.ID
		err := Zregister(instance, spl[1], id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Registered address "+spl[1]+" to user with ID "+m.Author.ID)
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zbalance" || spl[0] == "/balance" || spl[0] == "/balance@Zbot" {
		id := "discord " + m.Author.ID
		result, err := instance.Balance(nil, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Balance of user with id "+id+" is "+result.String())
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zaddress" || spl[0] == "/address" || spl[0] == "/address@Zbot" {
		id := "discord " + m.Author.ID
		result, err := instance.GetAddress(nil, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Address of user with id "+id+" is "+result.Hex())
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	} else if spl[0] == "Ztip" || spl[0] == "/tip" || spl[0] == "/tip@Zbot" {
		if len(spl) < 3 {
			s.ChannelMessageSend(m.ChannelID, "Not enough arguments: Ztip @recipient 5")
			return
		}
		if spl[1][0:2] != "<@" {
			s.ChannelMessageSend(m.ChannelID, "Malformed tag.")
			return
		}
		startid := 2
		if spl[1][0:3] == "<@!" || spl[1][0:3] == "<@&" {
			startid = 3
		}
		idrec := "discord " + spl[1][startid:len(spl[1])-1]
		idsender := "discord " + m.Author.ID
		amount, err := strconv.Atoi(spl[2])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Could not parse value.")
			return
		}
		amountbig := big.NewInt(int64(amount))
		bal, _ := instance.Balance(nil, idsender)
		if amountbig.Cmp(bal) > 0 || bal.Cmp(big.NewInt(0)) == 0 {
			s.ChannelMessageSend(m.ChannelID, "Not enough balance.")
			return
		}
		err = Ztip(instance, idsender, idrec, amountbig)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Tip request to user with id "+idrec+" sent to blockchain!")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zwithdraw" || spl[0] == "/withdraw" || spl[0] == "/withdraw@Zbot" {
		id := "discord " + m.Author.ID
		addr, _ := instance.GetAddress(nil, id)
		if addr.Hex() == "0x0000000000000000000000000000000000000000" {
			s.ChannelMessageSend(m.ChannelID, "No account registered with user ID, type Zregister 0x123YOURADDRESS")
			return
		}
		err := Zwithdraw(instance, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Withdrew all funds!")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zrain" || spl[0] == "/rain" || spl[0] == "/rain@Zbot" {
		dm, _ := ComesFromDM(s, m)
		if dm {
			s.ChannelMessageSend(m.ChannelID, "Cannot use rain in a DM.")
			return
		}
		g, err := s.State.Guild(m.GuildID)
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
		idsender := "discord " + m.Author.ID
		amount, err := strconv.Atoi(spl[1])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Could not parse value.")
			return
		}
		amountbig := big.NewInt(int64(amount))
		bal, _ := instance.Balance(nil, idsender)
		if amountbig.Cmp(bal) > 0 || bal.Cmp(big.NewInt(0)) == 0 {
			s.ChannelMessageSend(m.ChannelID, "Not enough balance.")
			return
		}
		if len(g.Presences) < 3 {
			s.ChannelMessageSend(m.ChannelID, "Not enough members online.")
			return
		}
		amount = amount / (len(g.Presences) - 2)
		amountbig = big.NewInt(int64(amount))
		for i := 0; i < len(g.Presences); i++ {
			if g.Presences[i].User.ID != "784363686873530369" && g.Presences[i].User.ID != m.Author.ID {
				idrec := "discord " + g.Presences[i].User.ID
				Ztip(instance, idsender, idrec, amountbig)
			}
		}
		s.ChannelMessageSend(m.ChannelID, "Rained tips on online members, each a value of "+strconv.Itoa(amount))
	} else if spl[0] == "Zhelp" || spl[0] == "/help" || spl[0] == "/help@Zbot" {
		s.ChannelMessageSend(m.ChannelID, "Please read the README at: https://gitlab.com/marcus.appelros/zbot")
	} else {
		dm, _ := ComesFromDM(s, m)
		if dm {
			s.ChannelMessageSend(m.ChannelID, "Try Zblock or read the README at: https://gitlab.com/marcus.appelros/zbot")
		}
	}
}

// ComesFromDM returns true if a message comes from a DM channel
func ComesFromDM(s *discordgo.Session, m *discordgo.MessageCreate) (bool, error) {
	channel, err := s.State.Channel(m.ChannelID)
	if err != nil {
		if channel, err = s.Channel(m.ChannelID); err != nil {
			return false, err
		}
	}

	return channel.Type == discordgo.ChannelTypeDM, nil
}
