package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/big"
	"net/http"
	"os"
	"strconv"
	"strings"
)

// Create a struct that mimics the webhook response body
// https://core.telegram.org/bots/api#update

//curl -F "url=https://e123.ngrok.io/"  https://api.telegram.org/bot123TOKEN/setWebhook
type webhookReqBody struct {
	Message struct {
		Text string `json:"text"`
		Chat struct {
			ID int64 `json:"id"`
		} `json:"chat"`
		From struct {
			ID         int64  `json:"id"`
			First_name string `json:"first_name"`
			Username   string `json:"username"`
		} `json:"from"`
		Entities []struct {
			User struct {
				ID         int64  `json:"id"`
				First_name string `json:"first_name"`
				Username   string `json:"username"`
			} `json:"user"`
		} `json:"entities"`
		Caption_entities []struct {
			User struct {
				ID         int64  `json:"id"`
				First_name string `json:"first_name"`
				Username   string `json:"username"`
			} `json:"user"`
		} `json:"caption_entities"`
	} `json:"message"`
}

// This handler is called everytime telegram sends us a webhook event
func Handler(res http.ResponseWriter, req *http.Request) {
	// First, decode the JSON response body
	body := &webhookReqBody{}
	if err := json.NewDecoder(req.Body).Decode(body); err != nil {
		fmt.Println("could not decode request body", err)
		return
	}

	spl := strings.Split(body.Message.Text, " ")
	if spl[0] == "Zblock" || spl[0] == "/block" || spl[0] == "/block@Zbot" {
		sayMessage(body.Message.Chat.ID, "The current Coston block is: "+currentBlock())
	} else if spl[0] == "Zregister" || spl[0] == "/register" || spl[0] == "/register@Zbot" {
		id := "telegram " + strconv.Itoa(int(body.Message.From.ID))
		if body.Message.From.Username != "" {
			id = "telegram " + body.Message.From.Username
		}
		err := Zregister(instance, spl[1], id)
		if err == nil {
			sayMessage(body.Message.Chat.ID, "Registered address "+spl[1]+" to user with ID "+id)
		} else {
			sayMessage(body.Message.Chat.ID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zbalance" || spl[0] == "/balance" || spl[0] == "/balance@Zbot" {
		id := "telegram " + strconv.Itoa(int(body.Message.From.ID))
		if body.Message.From.Username != "" {
			id = "telegram " + body.Message.From.Username
		}
		result, err := instance.Balance(nil, id)
		if err == nil {
			sayMessage(body.Message.Chat.ID, "Balance of user with id "+id+" is "+result.String())
		} else {
			sayMessage(body.Message.Chat.ID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zaddress" || spl[0] == "/address" || spl[0] == "/address@Zbot" {
		id := "telegram " + strconv.Itoa(int(body.Message.From.ID))
		if body.Message.From.Username != "" {
			id = "telegram " + body.Message.From.Username
		}
		result, err := instance.GetAddress(nil, id)
		if err == nil {
			sayMessage(body.Message.Chat.ID, "Address of user with id "+id+" is "+result.Hex())
		} else {
			sayMessage(body.Message.Chat.ID, "Error: "+err.Error())
		}
	} else if spl[0] == "Ztip" || spl[0] == "/tip" || spl[0] == "/tip@Zbot" {
		if len(spl) < 3 {
			sayMessage(body.Message.Chat.ID, "Not enough arguments: /tip @recipient 5")
			return
		}
		if len(body.Message.Entities) < 2 {
			return
		}
		idrec := "telegram " + strconv.Itoa(int(body.Message.Entities[1].User.ID))
		if idrec == "telegram 0" {
			idrec = "telegram " + spl[1][1:]
		}
		idsender := "telegram " + strconv.Itoa(int(body.Message.From.ID))
		if body.Message.From.Username != "" {
			idsender = "telegram " + body.Message.From.Username
		}
		amount, err := strconv.Atoi(spl[len(spl)-1])
		if err != nil {
			sayMessage(body.Message.Chat.ID, "Could not parse value.")
			return
		}
		amountbig := big.NewInt(int64(amount))
		bal, _ := instance.Balance(nil, idsender)
		if amountbig.Cmp(bal) > 0 || bal.Cmp(big.NewInt(0)) == 0 {
			sayMessage(body.Message.Chat.ID, "Not enough balance.")
			return
		}
		err = Ztip(instance, idsender, idrec, amountbig)
		if err == nil {
			sayMessage(body.Message.Chat.ID, "Tip request to user "+idrec+" sent to blockchain!")
		} else {
			sayMessage(body.Message.Chat.ID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zwithdraw" || spl[0] == "/withdraw" || spl[0] == "/withdraw@Zbot" {
		id := "telegram " + strconv.Itoa(int(body.Message.From.ID))
		if body.Message.From.Username != "" {
			id = "telegram " + body.Message.From.Username
		}
		addr, _ := instance.GetAddress(nil, id)
		if addr.Hex() == "0x0000000000000000000000000000000000000000" {
			sayMessage(body.Message.Chat.ID, "No account registered with user ID, type /register 0x123YOURADDRESS")
			return
		}
		if len(spl) > 1 {
			sayMessage(body.Message.Chat.ID, "Currently the only option is to withdraw all funds, to do this call /withdraw without any arguments.")
			return
		}
		err := Zwithdraw(instance, id)
		if err == nil {
			sayMessage(body.Message.Chat.ID, "Withdrew all funds!")
		} else {
			sayMessage(body.Message.Chat.ID, "Error: "+err.Error())
		}
	} else if spl[0] == "Zhelp" || spl[0] == "/help" || spl[0] == "/help@Zbot" {
		sayMessage(body.Message.Chat.ID, "Please read the README at: https://gitlab.com/marcus.appelros/zbot")
	} else {
		if spl[0] == "/start" {
			sayMessage(body.Message.Chat.ID, "Try /block or read the README at: https://gitlab.com/marcus.appelros/zbot")
		}
	}

	if !strings.Contains(strings.ToLower(body.Message.Text), "marco!") {
		return
	}

	// If the text contains marco, call the `sayPolo` function, which
	// is defined below
	if err := sayMessage(body.Message.Chat.ID, "Polo!!"); err != nil {
		fmt.Println("error in sending reply:", err)
		return
	}

	// log a confirmation message if the message is sent successfully
	fmt.Println("reply sent")
}

//The below code deals with the process of sending a response message
// to the user

// Create a struct to conform to the JSON body
// of the send message request
// https://core.telegram.org/bots/api#sendmessage
type sendMessageReqBody struct {
	ChatID int64  `json:"chat_id"`
	Text   string `json:"text"`
}

// sayPolo takes a chatID and sends "polo" to them
func sayMessage(chatID int64, msg string) error {
	// Create the request body struct
	reqBody := &sendMessageReqBody{
		ChatID: chatID,
		Text:   msg,
	}
	// Create the JSON body from the struct
	reqBytes, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}

	// Send a post request with your token
	res, err := http.Post("https://api.telegram.org/bot"+os.Getenv("TELEGRAM")+"/sendMessage", "application/json", bytes.NewBuffer(reqBytes))
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusOK {
		return errors.New("unexpected status" + res.Status)
	}

	return nil
}

func listenTelegram() {
	srv := &http.Server{
		Addr:    ":443",
		Handler: http.HandlerFunc(Handler),
	}
	log.Fatal(srv.ListenAndServeTLS("/etc/letsencrypt/live/triangulargo.club/fullchain.pem", "/etc/letsencrypt/live/triangulargo.club/privkey.pem"))
	//http.ListenAndServe(":3030", http.HandlerFunc(Handler))
}
